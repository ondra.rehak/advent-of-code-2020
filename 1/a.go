package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

const sumValue = 2020

func main() {
	file := loadFile()
	values := loadValues(file)
	defer file.Close()
	result, _ := processValues(values, sumValue)

	fmt.Println(result)
}

func loadFile() *os.File {
	file, err := os.Open("./1/input.txt")
	if err != nil {
		log.Fatal(err)
	}

	return file
}

func loadValues(file *os.File) []int {
	scanner := bufio.NewScanner(file)
	var values = []int{}
	for scanner.Scan() {
		line := scanner.Text()

		intValue, err := strconv.Atoi(line)
		if err != nil {
			log.Fatal(err)
		}
		values = append(values, intValue)
	}

	return values
}

func processValues(values []int, sum int) (int, error) {
	for i := 0; i < len(values); i++ {
		for j := 0; j < len(values); j++ {
			if values[i]+values[j] == sum {
				return values[i] * values[j], nil
			}
		}
	}

	return 0, fmt.Errorf("Sum values not found")
}
